import { Box, Flex, Spacer, Text } from '@chakra-ui/react'
import React from 'react'

export default function Product() {
  return (
    <Box
    w='320px'
    h='600px'
    >
        <Box
            border='1px solid black'
            height='320px'
            width='320px'
        >
        </Box>
        <Flex
        p={1}
        >
            <Text>Pear</Text>
            <Spacer></Spacer>
            <Text>$1.99</Text>
        </Flex>
        <Flex
        p='3px'
        >
            <Flex
            align='center'
            justify='center'
            border='1px solid black'
            w='20%' h='30px'
            borderRadius='30px'
            >
                <Text>1</Text>
            </Flex>
            <Spacer></Spacer>
            <Flex
            align='center'
            justify='center'
            border='1px solid black'
            w='60%' h='30px'
            borderRadius='30px'
            >
                <Text>Add to Cart</Text>
            </Flex>
        </Flex>
    </Box>
  )
}
