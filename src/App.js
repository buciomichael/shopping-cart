import './App.css';
import { Box, ChakraProvider } from '@chakra-ui/react';
import Welcome from './layout/Welcome';
import ShippingBanner from './layout/ShippingBanner';
import Navbar from './layout/Navbar';
import Product from './Product';

function App() {
  return (
    <ChakraProvider>
      <Navbar />
      <Welcome />
      <ShippingBanner />
      <Product />
    </ChakraProvider>
  );
}

export default App;
