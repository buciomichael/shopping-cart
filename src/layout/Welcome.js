import { Box, Flex, Text, VStack } from '@chakra-ui/react'
import React from 'react'

export default function Welcome() {
  return (
    <>
        <Box>
            <Flex>
                <Box
                border='2px solid black'
                background='#ACC970'
                height='400px'
                width='50%'
                ></Box>
                <Flex
                border='2px solid black'
                height='400px'
                width='50%'
                align='center'
                >
                    <VStack
                    p={10}
                    align='stretch'
                    direction='column'
                    >
                        <Text fontSize='1.8em' fontWeight={800}>Always Organic 🍈 Always Convenient</Text>
                        <Text fontSize='1.8em' fontWeight={800}>Delivered anywhere, anytime</Text>
                        <Text>Feed your body the natural way</Text>
                    </VStack>
                </Flex>
            </Flex>
        </Box>
    </>
  )
}
